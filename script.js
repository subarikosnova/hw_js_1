//============================================================================================================================================================
// 1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// Прототипне наслідування в JavaScript - це механізм, за яким об'єкти можуть успадковувати властивості і методи від інших об'єктів через їхні прототипи.
// Коли властивість чи метод не знаходиться в самому об'єкті, JavaScript шукає його в його прототипі і так далі.
//============================================================================================================================================================
// 2. Для чого потрібно викликати super() у конструкторі класу-нащадка
// super() у конструкторі класу-нащадка, викликаэ конструктор батьківського класу, щоб забезпечити правильне налаштування об'єктаю.
// Використання super() у конструкторі є обов'язковим, якщо у батьківському класі є конструктор, і я хочу використати його функціонал або передати параметри.
//============================================================================================================================================================





class Employee {
 constructor(name, age , salary) {
     this._name = name
     this._age = age
     this._salary = salary
     }
    get name() {
     return this._name;
 }
    get age() {
     return this._age
    }
    get salary() {
     return this._salary
    }
    set salary(value) {
     this._salary = value;
    }
}


class Programmer extends Employee {
    constructor(name, age , salary , lang) {
        super(name,age,salary);
        this._lang = lang;
    }
    get lang() {
        return this._lang
    }
    set lang(value) {
        this._lang = value;
    }
    get salary() {
        return super.salary * 3;
}}

const programmer1 = new Programmer('David Shore', 25, 35000, ['Python','C++'])
const programmer2 = new Programmer('Omar Eps', 27, 55000, ['JavaScript','C#'])
const programmer3 = new Programmer('Lisa Edelstein', 39, 95000, ['NextJS','Angular'])




console.log("Programmer 1:");
console.log("Name:", programmer1.name);
console.log("Age:", programmer1.age);
console.log("Salary:", programmer1.salary);
console.log("Languages:", programmer1.lang);
console.log();

console.log("Programmer 2:");
console.log("Name:", programmer2.name);
console.log("Age:", programmer2.age);
console.log("Salary:", programmer2.salary);
console.log("Languages:", programmer2.lang);

console.log("Programmer 1:");
console.log("Name:", programmer3.name);
console.log("Age:", programmer3.age);
console.log("Salary:", programmer3.salary);
console.log("Languages:", programmer3.lang);
console.log();